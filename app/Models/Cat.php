<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'breed',
        'description',
        'status',
        'location_id',
    ];
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
