<?php

namespace App\Http\Controllers\Api;

use App\Models\Cat;
use Illuminate\Http\Request;
use App\Http\Resources\CatResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCatFormRequest;

class CatController extends Controller
{
    public function index(Request $request)
    {
        $data = new Cat;
        $data = $data->orderBy('name', 'asc');
        $data = $data->get();
        return CatResource::collection($data);
    }
    public function create(AddCatFormRequest $request)
    {
        $cat = new Cat;
        $cat->name = $request->name;
        $cat->breed = $request->breed;
        $cat->description = $request->description;
        $cat->location_id = $request->location;
        $cat->save();
        return response()->json($cat);
    }
    public function show($id)
    {
        $cat = Cat::find($id);
        return new CatResource($cat);
    }
    public function update($id, Request $request)
    {
        $cat = Cat::find($id);
        $cat->name = $request->name;
        $cat->breed = $request->breed;
        $cat->description = $request->description;
        $cat->save();
        return response()->json($cat);
    }
    public function destroy($id)
    {
        $cat = Cat::find($id)->delete();
        return response()->json($cat);
    }
}