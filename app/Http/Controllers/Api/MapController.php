<?php

namespace App\Http\Controllers\Api;

use App\Models\Cat;
use App\Models\Location;
use Illuminate\Http\Request;
use App\Http\Resources\MapResource;
use App\Http\Controllers\Controller;
use App\Http\Resources\LocationResource;

class MapController extends Controller
{
    public function index(Request $request)
    {
        $data = new Cat;
        $data = $data->with('location');
        $data = $data->orderBy('location_id', 'asc');
        $data = $data->get();
        return MapResource::collection($data);
    }

    public function location(Request $request)
    {
        $data = new Location;
        $data = $data->get();
        return LocationResource::collection($data);
    }
}
