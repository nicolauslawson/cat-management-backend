<?php

namespace Database\Seeders;

use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Location::create(['name' => 'Main House']);
        Location::create(['name' => 'Living Area']);
        Location::create(['name' => 'House 3']);
        Location::create(['name' => 'House 4']);
        Location::create(['name' => 'Basement']);
    }
}
