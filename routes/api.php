<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('cat', [App\Http\Controllers\Api\CatController::class, 'index']);
Route::post('cat', [App\Http\Controllers\Api\CatController::class, 'create']);
Route::get('cat/{id}', [App\Http\Controllers\Api\CatController::class, 'show']);
Route::put('cat/{id}', [App\Http\Controllers\Api\CatController::class, 'update']);
Route::delete('cat/{id}', [App\Http\Controllers\Api\CatController::class, 'destroy']);

Route::get('map', [App\Http\Controllers\Api\MapController::class, 'index']);
Route::get('map/location', [App\Http\Controllers\Api\MapController::class, 'location']);