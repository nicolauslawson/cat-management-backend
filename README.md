## Build Setup

```bash
$ composer install
$ cp .env.example .env

update database connection

$ php artisan migrate

$ php artisan db:seed

$ php artisan key:generate

$ php artisan serve